'use strict';
var ApplicationConfiguration = (function() {
    var applicationModuleName = 'angularjsapp';
    var applicationModuleVendorDependencies = ['ngResource', 'ngCookies', 'ngAnimate', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'ui.utils'];
    var registerModule = function(moduleName) {
        angular
            .module(moduleName, []);
        angular
            .module(applicationModuleName)
            .requires
            .push(moduleName);
    };

    return {
        applicationModuleName: applicationModuleName,
        applicationModuleVendorDependencies: applicationModuleVendorDependencies,
        registerModule: registerModule
    };
})();

'use strict';

angular
    .module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

angular
    .module(ApplicationConfiguration.applicationModuleName)
    .config(['$locationProvider',
        function($locationProvider) {
            $locationProvider.hashPrefix('!');
        }
    ]);
angular
    .element(document)
    .ready(function() {
        if (window.location.hash === '#_=_') {
            window.location.hash = '#!';
        }
        angular
            .bootstrap(document,
                [ApplicationConfiguration.applicationModuleName]);
    });

'use strict';

ApplicationConfiguration.registerModule('core');

'use strict';

angular
    .module('core')
    .config(['$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise('/');

                        $stateProvider
    .state('aliquot', {
        url: '/aliquot',
        templateUrl: 'modules/core/views/aliquot.html',
        controller: 'AliquotController'
    }).
state('home', {
                    url: '/',
                    templateUrl: 'modules/core/views/home.html',
                    controller: 'HomeController'
                });
        }
    ]);

'use strict';

angular
    .module('core')
    .service('ActiveIngredientService', [

        function() {
            this.get = function(){
                return [{
                    'active_ingredient': 'Dextrose'
                }];
            };
        }
    ]);

'use strict';

angular
    .module('core')
    .service('SoluteService', [

        function() {

            this.get = function(){
                return  [
                    {
                        'solute': 'Water',
                        'multiplier': 0
                }, {
                        'solute': 'Dextrose 5%',
                        'multiplier': 0.05
                }, {
                        'solute': 'Dextrose 10%',
                        'multiplier': 0.10
                }];
            };
        }
    ]);

'use strict';

angular
    .module('core')
    .service('SourceService', [

        function() {

            this.get = function(){
                return [
                        {
                            'value': 0,
                            'display': 'Please Choose'
                        },
                        {
                            'value': 0.5,
                            'display': '50% Solution'
                    }, {
                            'value': 0.7,
                            'display': '70% Solution'
                    }];
            };
        }
    ]);

'use strict';

angular
    .module('core')
    .controller('AliquotController', [
        '$scope',
        'SoluteService',
        'SourceService',
        'ActiveIngredientService',
        function($scope,Solutes,Sources,ActiveIngredients) {
            
            window.sc = $scope;
            
            $scope.active_ingredient = 'Dextrose';
            $scope.final_concentration = '';
            $scope.final_volume = '';
            $scope.active_ingredient_source = 0;
            $scope.solute = 'Dextrose 5%';
            $scope.solution_modifier = 0;
        
            $scope.messages = [];
        
            $scope.active_ingredients = ActiveIngredients.get();
        
            $scope.solutes = [];
            
            angular.forEach(Solutes.get(),function(solute,index){                            
                if( solute.solute != 'Water' && solute.solute != '' ){
                    $scope.solutes.push(solute);
                } 
            });
        
            $scope.dextrose_sources = Sources.get();
            
            $scope.getParts1 = function(){
                return Math.abs(parseFloat($scope.final_concentration) - (parseFloat($scope.solute) * 100));
            };
            
            $scope.getParts2 = function(){
                return Math.abs(parseFloat($scope.final_concentration) - (parseFloat($scope.active_ingredient_source) * 100));
                console.log($scope.parts2);
            };
            
            $scope.getSourceNeeded = function(){
                return ($scope.getParts1() * parseFloat($scope.final_volume)) / ($scope.getParts1() + $scope.getParts2());
                console.log($scope.sourceNeeded);
            };
        }
]);
'use strict';

angular
    .module('core')
    .controller('HomeController', [
        '$scope', 
        'SoluteService',
        'SourceService',
        'ActiveIngredientService',
        function ($scope,Solutes,Sources,ActiveIngredients) {
    $scope.active_ingredient = 'Dextrose';
    $scope.final_concentration = '';
    $scope.final_volume = '';
    $scope.active_ingredient_source = 0;
    $scope.solute = 'Water';
    $scope.solution_modifier = 0;

    $scope.messages = [];

    $scope.active_ingredients = ActiveIngredients.get();

    $scope.solutes = Solutes.get();

    $scope.dextrose_sources = Sources.get();

    $scope.getSoluteNeeded = function () {
        if ($scope.solute == 'Water') {
            return $scope.final_volume - $scope.getSolutionNeeded();
        }
        return $scope.final_volume;
    };

    $scope.getAmountNeeded = function () {
        return (($scope.final_concentration / 100) * $scope.final_volume) - $scope.getAlreadyThere();
    };

    $scope.getSolutionNeeded = function () {
        return ($scope.getAmountNeeded() / $scope.active_ingredient_source);
    };

    $scope.getVolumeProduced = function () {
        if ($scope.solute != 'Water') {
            return parseFloat($scope.final_volume) + $scope.getSolutionNeeded();
        }
        return $scope.final_volume;
    };

    $scope.getConcentration = function () {
        return ($scope.getAmountNeeded() + $scope.getAlreadyThere()) / parseFloat($scope.getVolumeProduced());
    };

    $scope.getAlreadyThere = function () {
        var current_solute = {};

        angular.forEach($scope.solutes, function (solute, key) {
            if (solute.solute == $scope.solute) {
                current_solute = solute;
            }
        });

        return $scope.final_volume * current_solute.multiplier;
    };
}]);
