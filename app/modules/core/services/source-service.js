'use strict';

/**
 * @ngdoc service
 * @name core.Services.SourceService
 * @description SourceService Service
 */
angular
    .module('core')
    .service('SourceService', [

        function() {

            this.get = function(){
                return [
                        {
                            'value': 0,
                            'display': 'Please Choose'
                        },
                        {
                            'value': 0.5,
                            'display': '50% Solution'
                    }, {
                            'value': 0.7,
                            'display': '70% Solution'
                    }];
            };
        }
    ]);
