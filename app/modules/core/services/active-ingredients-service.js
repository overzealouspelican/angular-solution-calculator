'use strict';

/**
 * @ngdoc service
 * @name core.Services.ActiveIngredientService
 * @description ActiveIngredients Service
 */
angular
    .module('core')
    .service('ActiveIngredientService', [

        function() {
            this.get = function(){
                return [{
                    'active_ingredient': 'Dextrose'
                }];
            };
        }
    ]);
