'use strict';

/**
 * @ngdoc service
 * @name core.Services.SoluteService
 * @description SoluteService Service
 */
angular
    .module('core')
    .service('SoluteService', [

        function() {

            this.get = function(){
                return  [
                    {
                        'solute': 'Water',
                        'multiplier': 0
                }, {
                        'solute': 'Dextrose 5%',
                        'multiplier': 0.05
                }, {
                        'solute': 'Dextrose 10%',
                        'multiplier': 0.10
                }];
            };
        }
    ]);
