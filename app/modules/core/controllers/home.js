/// <reference path="../../../../typings/angularjs/angular.d.ts"/>
'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('HomeController', [
        '$scope', 
        'SoluteService',
        'SourceService',
        'ActiveIngredientService',
        function ($scope,Solutes,Sources,ActiveIngredients) {
    //Defaults
    $scope.active_ingredient = 'Dextrose';
    $scope.final_concentration = '';
    $scope.final_volume = '';
    $scope.active_ingredient_source = 0;
    $scope.solute = 'Water';
    $scope.solution_modifier = 0;

    $scope.messages = [];

    $scope.active_ingredients = ActiveIngredients.get();

    $scope.solutes = Solutes.get();

    $scope.dextrose_sources = Sources.get();

    $scope.getSoluteNeeded = function () {
        if ($scope.solute == 'Water') {
            return $scope.final_volume - $scope.getSolutionNeeded();
        }
        return $scope.final_volume;
    };

    $scope.getAmountNeeded = function () {
        return (($scope.final_concentration / 100) * $scope.final_volume) - $scope.getAlreadyThere();
    };

    $scope.getSolutionNeeded = function () {
        return ($scope.getAmountNeeded() / $scope.active_ingredient_source);
    };

    $scope.getVolumeProduced = function () {
        if ($scope.solute != 'Water') {
            return parseFloat($scope.final_volume) + $scope.getSolutionNeeded();
        }
        return $scope.final_volume;
    };

    $scope.getConcentration = function () {
        return ($scope.getAmountNeeded() + $scope.getAlreadyThere()) / parseFloat($scope.getVolumeProduced());
    };

    $scope.getAlreadyThere = function () {
        var current_solute = {};

        angular.forEach($scope.solutes, function (solute, key) {
            if (solute.solute == $scope.solute) {
                current_solute = solute;
            }
        });

        return $scope.final_volume * current_solute.multiplier;
    };
}]);
