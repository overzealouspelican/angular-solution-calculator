'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.AliquotController
 * @description AliquotController
 * @requires ng.$scope
*/
angular
    .module('core')
    .controller('AliquotController', [
        '$scope',
        'SoluteService',
        'SourceService',
        'ActiveIngredientService',
        function($scope,Solutes,Sources,ActiveIngredients) {
            
            window.sc = $scope;
            
            $scope.active_ingredient = 'Dextrose';
            $scope.final_concentration = '';
            $scope.final_volume = '';
            $scope.active_ingredient_source = 0;
            $scope.solute = 'Dextrose 5%';
            $scope.solution_modifier = 0;
        
            $scope.messages = [];
        
            $scope.active_ingredients = ActiveIngredients.get();
        
            $scope.solutes = [];
            
            angular.forEach(Solutes.get(),function(solute,index){                            
                if( solute.solute != 'Water' && solute.solute != '' ){
                    $scope.solutes.push(solute);
                } 
            });
        
            $scope.dextrose_sources = Sources.get();
            
            $scope.getParts1 = function(){
                return Math.abs(parseFloat($scope.final_concentration) - (parseFloat($scope.solute) * 100));
            };
            
            $scope.getParts2 = function(){
                return Math.abs(parseFloat($scope.final_concentration) - (parseFloat($scope.active_ingredient_source) * 100));
                console.log($scope.parts2);
            };
            
            $scope.getSourceNeeded = function(){
                return ($scope.getParts1() * parseFloat($scope.final_volume)) / ($scope.getParts1() + $scope.getParts2());
                console.log($scope.sourceNeeded);
            };
        }
]);
